1. A wordle game consits of an input with any repeated letters in the string and output will be in the form of string which contains R: red, G: grey, Y: yellow.

2. The output is based on secret word. And only 6 chances are given to guess the secret word.

3. When the letter is present in the secret word and correctly positioned then it is G. If it is present in the secret word and not correctly positioned then it is Y. If it is not present then its R
